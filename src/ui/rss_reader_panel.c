/*
 * rss_reader_panel.c
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "../include/rss_reader_panel.h"

/* Helpful macro */
#define JUSTIFY_LABEL(lab) { gtk_label_set_justify(GTK_LABEL(lab), GTK_JUSTIFY_LEFT); \
                              gtk_misc_set_alignment(GTK_MISC(lab), 0.0, 0.0); }

G_DEFINE_TYPE(RssReaderPanel, rss_reader_panel, GTK_TYPE_BOX);

enum {
        PROP_0, PROP_ITEM, N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void update_ui(RssReaderPanel *self);


/* Initialisation */
static void rss_reader_panel_class_init(RssReaderPanelClass *klass)
{
        GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

        obj_properties[PROP_ITEM] =
        g_param_spec_pointer("item", "Item", "Item", G_PARAM_READWRITE);

        gobject_class->set_property = rss_reader_panel_set_property;
        gobject_class->get_property = rss_reader_panel_get_property;
        g_object_class_install_properties (gobject_class,
                                           N_PROPERTIES,
                                           obj_properties);
}

static void rss_reader_panel_init(RssReaderPanel *self)
{
        GtkWidget *title, *description, *link;

        /* Nice bit of padding */
        gtk_container_set_border_width(GTK_CONTAINER(self), 5);

        /* Title */
        title = gtk_label_new("");
        self->title = title;
        JUSTIFY_LABEL(title);
        gtk_label_set_use_markup(GTK_LABEL(title), TRUE);
        gtk_widget_set_margin_bottom(title, 5);
        gtk_widget_set_name(title, "title");
        gtk_container_add(GTK_CONTAINER(self), title);

        /* Description */
        description = gtk_label_new("");
        self->description = description;
        JUSTIFY_LABEL(description);
        gtk_label_set_line_wrap(GTK_LABEL(description), TRUE);
        gtk_container_add(GTK_CONTAINER(self), description);

        /* Link */
        link = gtk_link_button_new("");
        self->link = link;
        gtk_container_add(GTK_CONTAINER(self), link);

        gtk_widget_show_all(GTK_WIDGET(self));
}

static void rss_reader_panel_dispose(GObject *object)
{
        RssReaderPanel *self;

        self = RSS_READER_PANEL(object);

        g_object_unref(self->title);
        g_object_unref(self->description);
        g_object_unref(self->link);

        if (self->title_string)
                g_free(self->title_string);

        /* Destruct */
        G_OBJECT_CLASS (rss_reader_panel_parent_class)->dispose (object);
}

/* Update our user interface */
static void update_ui(RssReaderPanel *self)
{
        if (self->title_string)
                g_free(self->title_string);

        self->title_string = g_strdup_printf("<big>%s</big>", self->item->title);
        gtk_label_set_markup(GTK_LABEL(self->title), self->title_string);

        gtk_link_button_set_uri(GTK_LINK_BUTTON(self->link), self->item->link);
        gtk_button_set_label(GTK_BUTTON(self->link), "View on the web");

        gtk_label_set_text(GTK_LABEL(self->description), self->item->description);
}

static void rss_reader_panel_set_property(GObject *object,
                                           guint prop_id,
                                           const GValue *value,
                                           GParamSpec *pspec)
{
        RssReaderPanel *self;

        self = RSS_READER_PANEL(object);
        switch (prop_id) {
                case PROP_ITEM:
                        self->item = g_value_get_pointer((GValue*)value);
                        update_ui(self);
                        break;
                default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
                                prop_id, pspec);
                        break;
        }
}

static void rss_reader_panel_get_property(GObject *object,
                                           guint prop_id,
                                           GValue *value,
                                           GParamSpec *pspec)
{
        RssReaderPanel *self;

        self = RSS_READER_PANEL(object);
        switch (prop_id) {
                case PROP_ITEM:
                        g_value_set_pointer((GValue *)value, (gpointer)self->item);
                        break;
                default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
                                prop_id, pspec);
                        break;
        }
}
/* Utility; return a new RssReaderPanel */
RssReaderPanel* rss_reader_panel_new(RssItem *item)
{
        RssReaderPanel *panel;

        panel = g_object_new(RSS_READER_PANEL_TYPE, "orientation", GTK_ORIENTATION_VERTICAL, "item", item, NULL);
        return RSS_READER_PANEL(panel);
}
