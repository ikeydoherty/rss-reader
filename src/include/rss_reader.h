/*
 * rss_reader.h
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef rss_reader_h
#define rss_reader_h

#include <glib-object.h>
#include <rss_feed.h>
#include <rss_item.h>

typedef struct _RssReader RssReader;
typedef struct _RssReaderClass   RssReaderClass;

#define RSS_READER_TYPE (rss_reader_get_type())
#define RSS_READER(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), RSS_READER_TYPE, RssReader))
#define IS_RSS_READER(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RSS_READER_TYPE))
#define RSS_READER_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), RSS_READER_TYPE, RssReaderClass))
#define IS_RSS_READER_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), RSS_READER_TYPE))
#define RSS_READER_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), RSS_READER_TYPE, RssReaderClass))

typedef enum {
        RSS_CHANNEL_TITLE,
        RSS_CHANNEL_LINK,
        RSS_CHANNEL_DESCRIPTION,
        RSS_CHANNEL_COPYRIGHT,

        RSS_ITEM_TITLE,
        RSS_ITEM_LINK,
        RSS_ITEM_DESCRIPTION,

        RSS_CHANNEL_MAXVALUES
} RssFieldKey;

/* RssReader object */
struct _RssReader {
        GObject parent;

        RssFeed *feed;
};

/* RssReader class definition */
struct _RssReaderClass {
        GObjectClass parent_class;
};

/* Boilerplate GObject code */
static void rss_reader_class_init(RssReaderClass *klass);
static void rss_reader_init(RssReader *reader);
static void rss_reader_dispose(GObject *object);
GType rss_reader_get_type(void);

/* RssReader methods */
RssReader* rss_reader_new(void);

/* Parse the given RSS URI */
gboolean rss_reader_parse_uri(RssReader *reader, gchar *uri);

#endif /* rss_reader_h */
