/*
 * rss_reader_panel.h
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef rss_reader_panel_h
#define rss_reader_panel_h

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include "../include/rss_item.h"

typedef struct _RssReaderPanel RssReaderPanel;
typedef struct _RssReaderPanelClass   RssReaderPanelClass;

#define RSS_READER_PANEL_TYPE (rss_reader_panel_get_type())
#define RSS_READER_PANEL(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), RSS_READER_PANEL_TYPE, RssReaderPanel))
#define IS_RSS_READER_PANEL(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RSS_READER_PANEL_TYPE))
#define RSS_READER_PANEL_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), RSS_READER_PANEL_TYPE, RssReaderPanelClass))
#define IS_RSS_READER_PANEL_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), RSS_READER_PANEL_TYPE))
#define RSS_READER_PANEL_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), RSS_READER_PANEL_TYPE, RssReaderPanelClass))

/* RssReaderPanel object */
struct _RssReaderPanel {
        GtkBox parent;

        gchar *title_string;
        GtkWidget *title;
        GtkWidget *description;
        GtkWidget *link;
        RssItem *item;
};

/* RssReaderPanel class definition */
struct _RssReaderPanelClass {
        GtkBoxClass parent_class;
};

/* Boilerplate GObject code */
static void rss_reader_panel_class_init(RssReaderPanelClass *klass);
static void rss_reader_panel_init(RssReaderPanel *reader);
static void rss_reader_panel_dispose(GObject *object);
static void rss_reader_panel_set_property(GObject *object,
                                           guint prop_id,
                                           const GValue *value,
                                           GParamSpec *pspec);
static void rss_reader_panel_get_property(GObject *object,
                                           guint prop_id,
                                           GValue *value,
                                           GParamSpec *pspec);
GType rss_reader_panel_get_type(void);

/* RssReaderPanel methods */
RssReaderPanel* rss_reader_panel_new(RssItem *item);

#endif /* rss_reader_panel_h */
