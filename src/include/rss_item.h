/*
 * rss_item.h
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef rss_item_h
#define rss_item_h

#include <glib-object.h>

typedef struct _RssItem RssItem;
typedef struct _RssItemClass   RssItemClass;

#define RSS_ITEM_TYPE (rss_item_get_type())
#define RSS_ITEM(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), RSS_ITEM_TYPE, RssItem))
#define IS_RSS_ITEM(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RSS_ITEM_TYPE))
#define RSS_ITEM_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), RSS_ITEM_TYPE, RssItemClass))
#define IS_RSS_ITEM_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), RSS_ITEM_TYPE))
#define RSS_ITEM_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), RSS_ITEM_TYPE, RssItemClass))

/* RssItem object */
struct _RssItem {
        GObject parent;

        /* Probably going private too */
        gchar *title;
        gchar *description;
        gchar *link;

        /* Need to add date, guid, media */
};

/* RssItem class definition */
struct _RssItemClass {
        GObjectClass parent_class;
};

/* Boilerplate GObject code */
static void rss_item_class_init(RssItemClass *klass);
static void rss_item_init(RssItem *reader);
static void rss_item_dispose(GObject *object);
GType rss_item_get_type(void);

/* RssItem methods */
RssItem* rss_item_new(void);

#endif /* rss_item_h */
