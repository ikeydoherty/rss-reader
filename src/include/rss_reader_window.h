/*
 * rss_reader_window.h
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef rss_reader_window_h
#define rss_reader_window_h

#include <glib-object.h>
#include <gtk/gtk.h>

#include "../include/rss_reader.h"

/* Icon definition */
#define REFRESH_ICON "view-refresh"

typedef struct _RssReaderWindow RssReaderWindow;
typedef struct _RssReaderWindowClass   RssReaderWindowClass;
typedef struct _RssReaderWindowPrivate RssReaderWindowPrivate;

#define RSS_READER_WINDOW_TYPE (rss_reader_window_get_type())
#define RSS_READER_WINDOW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), RSS_READER_WINDOW_TYPE, RssReaderWindow))
#define IS_RSS_READER_WINDOW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RSS_READER_WINDOW_TYPE))
#define RSS_READER_WINDOW_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), RSS_READER_WINDOW_TYPE, RssReaderWindowClass))
#define IS_RSS_READER_WINDOW_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), RSS_READER_WINDOW_TYPE))
#define RSS_READER_WINDOW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), RSS_READER_WINDOW_TYPE, RssReaderWindowClass))

/* Private storage */
struct _RssReaderWindowPrivate {
        GtkWidget *window;

        GtkIconTheme *icon_theme;

        /* Our Rss parser */
        RssReader *reader;
        gchar *current_uri;

        /* Select RSS feeds */
        GtkWidget *combo;
        GtkListStore *store;

        /* Main display */
        GtkWidget *listbox;

        /* Header widgets */
        GtkWidget *header;
        GtkWidget *refresh_button;
        GtkWidget *refresh_image;
        GdkPixbuf *refresh_pixbuf;
};

/* RssReaderWindow object */
struct _RssReaderWindow {
        GObject parent;

        RssReaderWindowPrivate *priv;
};

/* RssReaderWindow class definition */
struct _RssReaderWindowClass {
        GObjectClass parent_class;
};


/* Boilerplate GObject code */
static void rss_reader_window_class_init(RssReaderWindowClass *klass);
static void rss_reader_window_init(RssReaderWindow *reader);
static void rss_reader_window_dispose(GObject *object);
GType rss_reader_window_get_type(void);

/* RssReaderWindow methods */
RssReaderWindow* rss_reader_window_new(void);

#endif /* rss_reader_window_h */
