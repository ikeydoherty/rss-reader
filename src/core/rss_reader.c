/*
 * rss_reader.c
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include <glib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "../include/rss_reader.h"

G_DEFINE_TYPE(RssReader, rss_reader, G_TYPE_OBJECT);

static void set_field(gpointer which, xmlDocPtr document, xmlNode *node, gchar *compare, RssFieldKey key);

/* Initialisation */
static void rss_reader_class_init(RssReaderClass *klass)
{
        /* Nothing yet */
}

static void rss_reader_init(RssReader *self)
{
        LIBXML_TEST_VERSION
}

static void rss_reader_dispose(GObject *object)
{
        RssReader *self;

        self = RSS_READER(object);

        xmlCleanupParser();

        if (self->feed)
                g_object_unref(self->feed);

        /* Destruct */
        G_OBJECT_CLASS (rss_reader_parent_class)->dispose (object);
}

/* Utility; return a new RssReader */
RssReader* rss_reader_new(void)
{
        RssReader *reader;

        reader = g_object_new(RSS_READER_TYPE, NULL);
        return RSS_READER(reader);
}

gboolean rss_reader_parse_uri(RssReader *self, gchar *uri)
{
        gboolean ret = FALSE;
        xmlDocPtr document;
        xmlNode *root_element, *curNode, *itemNode;
        xmlNode *rss, *channel;
        RssItem *rssItem;

        if (self->feed) {
                g_object_unref(self->feed);
                self->feed = NULL;
        }

        /* Create a new feed object */
        self->feed = rss_feed_new();

        /* Load the document */
        document = xmlReadFile(uri, NULL, 0);
        if (!document)
                goto end;
        /* Now attempt to parse the XML file */
        root_element = xmlDocGetRootElement(document);
        if (!root_element) {
                /* Raise an error, this shouldn't happen */
                goto fail;
        }

        /* Check the first item is 'rss' */
        rss = root_element->children->next;
        if (rss->type != XML_ELEMENT_NODE && g_strcmp0(rss->name, "rss") != 0)
                goto fail;

        /* Parse the channel section */
        channel = rss->children->next;
        if (channel->type != XML_ELEMENT_NODE && g_strcmp0(channel->name, "channel") != 0)
                goto fail;

        for (curNode = channel; curNode; curNode = curNode->next) {
                if (curNode->type == XML_ELEMENT_NODE) {
                        if (g_strcmp0(curNode->name, "item") == 0) {
                                rssItem = rss_item_new();
                                for (itemNode = curNode->children; itemNode; itemNode = itemNode->next) {
                                        if (itemNode->type != XML_ELEMENT_NODE)
                                                continue;
                                        set_field(rssItem, document, itemNode, "description", RSS_ITEM_DESCRIPTION);
                                        set_field(rssItem, document, itemNode, "title", RSS_ITEM_TITLE);
                                        set_field(rssItem, document, itemNode, "link", RSS_ITEM_LINK);
                                }
                                self->feed->items = g_list_append(self->feed->items, rssItem);
                        }
                        if (!self->feed->description)
                                set_field(self, document, curNode, "description", RSS_CHANNEL_DESCRIPTION);
                        if (!self->feed->title)
                                set_field(self, document, curNode, "title", RSS_CHANNEL_TITLE);
                        if (!self->feed->link)
                                set_field(self, document, curNode, "link", RSS_CHANNEL_LINK);
                        if (!self->feed->copyright)
                                set_field(self, document, curNode, "copyright", RSS_CHANNEL_COPYRIGHT);
                }
        }
        ret = TRUE;

fail:
        xmlFreeDoc(document);

end:
        return ret;
}

static void set_field(gpointer which, xmlDocPtr document, xmlNode *node, gchar *compare, RssFieldKey key)
{
        RssReader *self;
        RssItem *item;
        xmlChar *text;

        if (IS_RSS_READER(which)) {
                self = RSS_READER(which);
                if (g_strcmp0(node->name, compare) == 0) {
                        text = xmlNodeListGetString(document, node->xmlChildrenNode, 1);

                        switch (key) {
                                case RSS_CHANNEL_DESCRIPTION:
                                        self->feed->description = g_strdup(text);
                                        break;
                                case RSS_CHANNEL_TITLE:
                                        self->feed->title = g_strdup(text);
                                        break;
                                case RSS_CHANNEL_COPYRIGHT:
                                        self->feed->copyright = g_strdup(text);
                                        break;
                                case RSS_CHANNEL_LINK:
                                        self->feed->link = g_strdup(text);
                                        break;
                                default:
                                        break;
                        }
                        xmlFree(text);
                }
        } else {
                item = RSS_ITEM(which);
                if (g_strcmp0(node->name, compare) == 0) {
                        text = xmlNodeListGetString(document, node->xmlChildrenNode, 1);
                        switch (key) {
                                case RSS_ITEM_DESCRIPTION:
                                        item->description = g_strdup(text);
                                        break;
                                case RSS_ITEM_TITLE:
                                        item->title = g_strdup(text);
                                        break;
                                case RSS_ITEM_LINK:
                                        item->link = g_strdup(text);
                                        break;
                                default:
                                        break;
                        }
                        xmlFree(text);
                }
        }
}
