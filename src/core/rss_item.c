/*
 * rss_item.c
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "../include/rss_item.h"

G_DEFINE_TYPE(RssItem, rss_item, G_TYPE_OBJECT);

/* Initialisation */
static void rss_item_class_init(RssItemClass *klass)
{
        /* Nothing yet */
}

static void rss_item_init(RssItem *self)
{
        /* Nothing yet */
}

static void rss_item_dispose(GObject *object)
{
        RssItem *self;

        self = RSS_ITEM(object);

        /* Cleanup our fields */
        if (self->description) {
                g_free(self->description);
                self->description = NULL;
        }
        if (self->title) {
                g_free(self->title);
                self->title = NULL;
        }
        if (self->link) {
                g_free(self->link);
                self->link = NULL;
        }

        /* Destruct */
        G_OBJECT_CLASS (rss_item_parent_class)->dispose (object);
}

/* Utility; return a new RssItem */
RssItem* rss_item_new(void)
{
        RssItem *item;

        item = g_object_new(RSS_ITEM_TYPE, NULL);
        return RSS_ITEM(item);
}
