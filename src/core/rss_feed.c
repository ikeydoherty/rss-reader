/*
 * rss_feed.c
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "../include/rss_feed.h"

G_DEFINE_TYPE(RssFeed, rss_feed, G_TYPE_OBJECT);

static void destroy_item(gpointer item);

/* Initialisation */
static void rss_feed_class_init(RssFeedClass *klass)
{
        /* Nothing yet */
}

static void rss_feed_init(RssFeed *self)
{
        self->items = NULL;
}

static void rss_feed_dispose(GObject *object)
{
        RssFeed *self;

        self = RSS_FEED(object);

        /* Cleanup the rss feed object */
        if (self->description) {
                g_free(self->description);
                self->description = NULL;
        }
        if (self->title) {
                g_free(self->title);
                self->title = NULL;
        }
        if (self->copyright) {
                g_free(self->copyright);
                self->copyright = NULL;
        }
        if (self->link) {
                g_free(self->link);
                self->link = NULL;
        }
        if (self->items) {
                g_list_free_full(self->items, &destroy_item);
                self->items = NULL;
        }
        
        /* Destruct */
        G_OBJECT_CLASS (rss_feed_parent_class)->dispose (object);
}

static void destroy_item(gpointer item)
{
        g_object_unref(item);
        item = NULL;
}

/* Utility; return a new RssFeed */
RssFeed* rss_feed_new(void)
{
        RssFeed *item;

        item = g_object_new(RSS_FEED_TYPE, NULL);
        return RSS_FEED(item);
}
