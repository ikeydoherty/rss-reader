Simple RSS reader for GNOME3.10
-------------------------------

Incredibly simplistic RSS reader that does nothing more than let you
view selected RSS feeds. The user can view the news item by clicking
the provided link for each item.

RssReader is written entirely in C, with the intention of being as fast
as possible. It uses libxml2 to parse the RSS feeds, and targets
GNOME3.10 to ensure integration in Numix DX.

Bugs
====
Well, there would be, right? :) This is a brand new and part-time
project, so don't expect the world to change overnight. However, if you
find a bug, please do report it so I can fix it!

 * HTTPS doesnt work (check The Library section below)
 * No default timeout (Again, check The Library)
 * No async operation (planned)
 * No handling of HTML <item>'s in RSS feeds. It just shouldn't be there
   anyway. But hey, Linux kernel and Steam use it.. so we'll have to
   implement the support.

Naming
====
Everything is in the 'Rss' namespace, because I can't think of a decent
name yet. This will change in the future, suggestions would be great!

The Library
====
I decided it would be too much overhead to use an external library, and
wrote a **very** small parsing library. This in itself currently has
limitations, in that it directly uses libxml2 to open remote URI's. In
the near future this will be changed to use GIO to stream the remote
feed. This means HTTPS doesn't work yet!!

Authors
========
 * Ikey Doherty <ikey@solusos.com>

License: GPLv2+
